public class FindGrade{
    public static void main(String args[]){
        int not = Integer.parseInt(args[0]);
        if (not < 60){
            System.out.println("Grade : F");
        }
        else if (not < 70) {
            System.out.println("Grade : D");                    
        }
        else if (not < 80) {
            System.out.println("Grade : C");                    
        }
        else if (not < 90) {
            System.out.println("Grade : B");
        }
        else if (not < 100) {
            System.out.println("Grade : A");                    
        }
    }
}

import java.util.*;

public class FindSecondMax{
  public static void main(String[] args){
    Scanner reader = new Scanner(System.in);

    System.out.print("Write a number how many numbers you will enter: ");
    int howMany = reader.nextInt(); //  how many number user will enter
    int[] numberList = new int[howMany]; //  it's for keeping the numbers user entered

    System.out.println("For exit hit '-1'");
    for (int i = 0; i < howMany; i++){
      System.out.println("Write a number: ");
      int number = reader.nextInt();
      if (number == -1){
        break;
      }
      numberList[i] = number;
    }
  }
}

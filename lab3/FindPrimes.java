import java.util.Scanner;

public class FindPrimes{
    public static void main(String[] args){
        Scanner reader = new Scanner(System.in);
        
        System.out.println("Write a number to see the prime numbers until your number.");
        int number = reader.nextInt();               
        boolean control = true;
        while (true){
            for (int i = 2; i < number; i++){
                control = true;
                for (int j = 2; j < i; j++){
                    if (i % j == 0){
                        control = false;
                        break;
                    }
                }
                if (control == true){
                    System.out.println("This number is prime: " + i);
                }
            } 
            break; 
        }
    }
}


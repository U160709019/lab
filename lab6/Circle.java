public class Circle {
	int radius;
	Point center;
	
	public Circle(int r ,Point c ) {
		radius = r;
		center = c;
	}
	public double area() {
		return Math.PI * radius * radius ;
		
	}
}


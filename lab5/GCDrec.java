import java.util.Scanner;

class GCDrec{
    public static void main(String[] args){
        Scanner reader = new Scanner(System.in);
        int firstNumber, secondNumber;
            
        System.out.print("First number: ");
        firstNumber = reader.nextInt();

        System.out.print("Second number: ");
        secondNumber = reader.nextInt();

        System.out.println(firstNumber > secondNumber ? GCDwithRec(firstNumber, secondNumber) : GCDwithRec(secondNumber, firstNumber));
    }

    public static int GCDwithRec(int number1, int number2){

        /*
        60 18

        n1 n2 r
        60 18 6
        18 6  0
        */
        if (number2 == 0) return number1;

        else return GCDwithRec(number2, number1 % number2);
    }
}


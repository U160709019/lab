package lab;

public class Stackitem {
	Object obj;
	
	Stackitem next;

	public Stackitem(Object obj) {
		this.obj= obj;
	}

	public Stackitem getNext() {
		return next;
	}

	public void setNext(Stackitem next) {
		this.next = next;
	}

	public Object getObj() {
		return obj;
	}

	public void setObj(Object obj) {
		this.obj = obj;
	}




}

